package com.devcamp.task79;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task79Application {

	public static void main(String[] args) {
		SpringApplication.run(Task79Application.class, args);
	}

}
