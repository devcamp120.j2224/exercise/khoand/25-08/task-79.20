package com.devcamp.task79.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task79.entity.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {

}
